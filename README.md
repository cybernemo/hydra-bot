# hydra-bot

Crypto-bot using Kraken API using Python 3.

# Installation

```bash
# clone project
git clone https://gitlab.com/cybernemo/hydra-bot.git
cd hydra-bot
# bootstrap virtualenv
export VIRTUAL_ENV=.venv/hydra-bot
mkdir -p $VIRTUAL_ENV
virtualenv $VIRTUAL_ENV
source $VIRTUAL_ENV/bin/activate
# install from PyPI
pip install -r requirements.txt
```
